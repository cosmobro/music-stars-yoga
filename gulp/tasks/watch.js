'use strict';

module.exports = function () {
  $.gulp.task('watch', function () {
    $.gulp.watch('./source/js/**/*.js', $.gulp.series('js:process'));
    $.gulp.watch('./source/style/**/*.scss', $.gulp.series('sass'));
    $.gulp.watch('./source/template/**/*.pug', $.gulp.series('pug'));
    $.gulp.watch(['./source/images/**/*.*', '!./source/images/sprite/**/*.*'], $.gulp.series('copy:image'));
    $.gulp.watch('./source/images/sprite/scrn1/**/*.png', $.gulp.series('sprite:img'));
    $.gulp.watch('./source/php/**/*.*', $.gulp.series('copy:php'));
    $.gulp.watch('./source/fonts/**/*.*', $.gulp.series('copy:fonts'));
    $.gulp.watch('./source/other/**/*.*', $.gulp.series('copy:other'));
  });
};
