'use strict';

module.exports = function () {
  var buffer = require('vinyl-buffer');
  var merge = require('merge-stream');
  $.gulp.task('sprite:img', function () {
    var spriteData = $.gulp.src('./source/images/sprite/**/*.png').pipe($.gp.spritesmith({
      imgName: '../img/sprite.png',
      cssName: 'sprite.scss'
    }));

    var imgStream = spriteData.img
      .pipe(buffer())
      .pipe($.gp.imagemin($.config.imageminOptions))
      .pipe($.gulp.dest($.config.root + '/assets/img'));

    var cssStream = spriteData.css
      .pipe($.gulp.dest('./source/style/helpers/mixins'));

    return merge(imgStream, cssStream);
  });
};