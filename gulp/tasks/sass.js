'use strict';

module.exports = function () {
  $.gulp.task('sass', function () {
    return $.gulp.src('./source/style/app.scss')
      .pipe($.gp.sourcemaps.init())
      .pipe($.gp.sass({outputStyle: 'expanded'})).on('error', $.gp.notify.onError({title: 'Style'}))
      .pipe($.gp.sourcemaps.write({
        includeContent: false
      }))
      // TODO: попробовать отключить автопрефиксер
      .pipe($.gp.autoprefixer({browsers: $.config.autoprefixerConfig}))
      .pipe($.gulp.dest($.config.root + '/assets/css'))
      .pipe($.browserSync.stream());
  })
};
