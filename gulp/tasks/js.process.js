'use strict';

module.exports = function () {
  $.gulp.task('js:process', function () {
    return $.gulp.src($.path.app)
      .pipe($.gp.browserify({
        debug: true
      }).on('error', $.gp.notify.onError({title: 'JS'})))
      .pipe($.gulp.dest($.config.root + '/assets/js'))
  });
};
