'use strict';

module.exports = function () {
  $.gulp.task('serve:php', function () {
    $.gp.connectPhp.server({
      base: $.config.root,
      hostname: 'localhost',
      port: 8080,
      keepalive: true,
      open: false
    })
  });
};
