'use strict';

module.exports = function () {
  $.gulp.task('serve', function () {
    $.browserSync.init({
      open: false,
      proxy: 'localhost:8080'
    });

    $.browserSync.watch([$.config.root + '/**/*.*', '!**/*.css'], $.browserSync.reload);
  });
};
