'use strict';

var $ = require('jquery');
var menu = require('./common/menu');
var photoSlider = require('./common/photo-slider');
var coachSlider = require('./common/coach-slider');
var map = require('./common/map');
var toggleMap = require('./common/toggle-map');
var sendForm = require('./common/send-form');
var popups = require('./common/popups');
var scrollTo = require('./common/scroll-to');

$(function () {
  sendForm();
  menu();
  photoSlider();
  coachSlider();
  map.init();
  toggleMap();
  popups();
  scrollTo();
});

