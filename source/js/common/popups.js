'use strict';

var $ =require('jquery');
require('magnific-popup');

function init() {
  var $galleries = $('.js-popup__gallery');
  var $popupOpeners = $('.js-popup__opener');
  var $closeButton = $('.js-popup__close');
  var magnificPopupOptions = {
    type: 'inline',
    midClick: true,
    showCloseBtn: false,
    removalDelay: 200,
    mainClass: 'mfp-fade',
    fixedContentPos: true,
    callbacks: {
      afterClose: closePopupHandler
    }
  };
  var magnificPopupImageOptions = {
    delegate: '.js-popup__opener_image',
    type: 'image',
    midClick: true,
    removalDelay: 200,
    mainClass: 'mfp-fade',
    fixedContentPos: true,
    gallery:{
      enabled: true,
      tCounter: '<span class="mfp-counter">%curr% из %total%</span>'
    }
  };

  function closePopupHandler(e) {
    if (e && typeof e.preventDefault !== 'undefined') {
      e.preventDefault();
    }
    // Очищаем таймер закрытия окна
    clearTimeout(window.timerToCloseMsgPopup);

    $.magnificPopup.close();
  }

  $popupOpeners.magnificPopup(magnificPopupOptions);
  $galleries.each(function () {
    var $this = $(this);

    $this.magnificPopup(magnificPopupImageOptions);
  });

  $closeButton.click(closePopupHandler);
}

module.exports = init;