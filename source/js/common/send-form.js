'use strict';

var $ = require('jquery');
require('jquery-placeholder');
require('jquery-validation');
var mask = require('../../../node_modules/jquery.maskedinput/src/jquery.maskedinput.js');

function sendForm() {
  var $inputs = $('input');
  var validationRules = {
    name: {
      minlength: 3,
      required: true,
      notPlaceholder: true
    },
    phone: {
      phoneTest: true,
      phoneRequired: true,
      required: true,
      notPlaceholder: true
    }
  };
  var validationMessages = {
    name: {
      minlength: 'Минимум 3 символа',
      required: 'Вы не ввели имя',
      notPlaceholder: 'Вы не ввели имя'
    },
    phone: {
      phoneTest: 'Некорректный телефон',
      phoneRequired: 'Вы не ввели телефон',
      required: 'Вы не ввели телефон',
      notPlaceholder: 'Вы не ввели телефон'
    }
  };
  var $thanksPopup = $('#popup-thanks');
  var $errorPopup = $('#popup-error');
  var timeToClose = 7000;
  var popupSettings = {
    items: {
      src: null
    },
    type: 'inline',
    midClick: true,
    showCloseBtn: false,
    removalDelay: 200,
    mainClass: 'mfp-fade',
    fixedContentPos: true
  };
  // Таймер закрытия окна
  window.timerToCloseMsgPopup = null;

  // Ввод по маске
  $('input[type="tel"]').mask("+7 999 999 99 99");
  $inputs.placeholder();

  // Методы валидатора
  // Проверка на подсказку, т.к. фикс плейсхолдера конфликтует с валидатором
  $.validator.addMethod('notPlaceholder', function (val, el) {
    return this.optional(el) || (val !== $(el).attr('placeholder'));
  }, $.validator.messages.required);

  // Проверка на телефон
  $.validator.addMethod('phoneTest', function (val, el) {
    if (val == '+7 ___ ___ __ __' || val == '' || val == $(el).attr('placeholder')) {
      return true;
    }
    return /^\+7\s\d{3,3}\s\d{3,3}\s\d{2,2}\s\d{2,2}$/i.test(val);
  }, $.validator.messages.required);

  // Проверка на обязательность телефона
  $.validator.addMethod('phoneRequired', function (val, el) {
    return val == '+7 ___ ___ __ __' && $(el).closest('form').hasClass('isTry2Submit') ? false : true;
  }, $.validator.messages.required);

  // Показыватель ошибок
  function validationShowErrorsHandler(errorMap, errorList) {
    var submitButton = $(this.currentForm).find('button, input:submit');
    for (var error in errorList) {
      var element = $(errorList[error].element),
          errorMessage = errorList[error].message;
      element.val(errorMessage).addClass('error').attr('placeholder', errorMessage);
    }
  }

  // Запихиватель ошибок в плейсхолдер
  function validationErrorPlacementHandler(error, element) {
    element.attr("placeholder", error[0].outerText);
  }

  // Открывает попап
  function openPopup(popup) {
    popupSettings.items.src = popup;
    $.magnificPopup.open(popupSettings, 0);
  }

  // Ставит таймер закрытия окна
  function setClosePopupTimer() {
    window.timerToCloseMsgPopup = setTimeout(function () {
      $.magnificPopup.close();
    }, timeToClose);
  }

  // Обработчик успеха отправки
  function submitSuccessHandler(data) {
    openPopup($thanksPopup);

    setClosePopupTimer();
  }

  // Обработчик ошибки отправки
  function submitErrorHandler(data) {
    openPopup($errorPopup);

    setClosePopupTimer();
  }

  function sendForm(data, form) {
    // Цель
    var goal = $(form).data('goal');

    // Форма отправляется
    $(form).addClass('sending');

    // Обработчик завершения отправки
    function submitCompleteHandler() {
      // Цель яндекса
      yaCounter39646625.reachGoal(goal);
      ga('send', {
        hitType: 'event',
        eventCategory: 'form',
        eventAction: 'send',
        eventLabel: goal
      });

      // Форма отправилась
      $(form).removeClass('sending');
    }

    $.ajax({
      type: 'POST',
      url: 'php/send.php',
      data: data,
      success: submitSuccessHandler,
      error: submitErrorHandler,
      complete: submitCompleteHandler
    });
  }

  // Обработчик отправки формы
  function submitHandler(form) {
    sendForm($(form).serialize(), form);
  }

  // Валидация отправки
  $('.js-form__ajax').each(function () {
    $(this).validate({
      rules: validationRules,
      messages: validationMessages,
      errorPlacement: validationErrorPlacementHandler,
      showErrors: validationShowErrorsHandler,
      submitHandler: submitHandler,
      errorClass: 'error',
      validClass: 'valid',
      errorElement: 'span',
      onkeyup: false
    });
  });

  $inputs.on('focus', function () {
    if ($(this).hasClass('error')) {
      $(this).val('').removeClass('error');
    }
  });

  $('button').click(function () {
    $(this).closest('form').addClass('isTry2Submit');
  });
}

module.exports = sendForm;