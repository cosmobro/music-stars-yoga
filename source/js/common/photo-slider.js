'use strict';

var $ = require('jquery');
require('slick-carousel');

function init() {
  var $slider = $('.js-photo-slider');
  var $sliderTrack = $slider.find('.js-photo-slider__list');
  var $nextButton = $slider.find('.js-photo-slider__nav_next');
  var $prevButton = $slider.find('.js-photo-slider__nav_prev');
  var $dotsContainer = $slider.find('.js-photo-slider__dots');

  var sliderOptions = {
    prevArrow: $prevButton,
    nextArrow: $nextButton,
    variableWidth: true,
    centerMode: true,
    slidesToShow: 1,
    dots: true,
    appendDots: $dotsContainer,
    customPaging: function (slider, i) {
      return '<div class="photo-slider__dot"></div>'
    }
  };

  $sliderTrack.slick(sliderOptions);
}

module.exports = init;