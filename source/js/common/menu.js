'use strict';

var $ = require('jquery');

function init() {
  var $menu = $('.js-menu');
  var $menuCheckbox = $menu.find('.js-menu__checkbox');

  function toggleMenu(e) {
    var $this = $(this);

    if ($this.is(':checked')) {
      $menu.addClass('active');
    } else {
      $menu.removeClass('active');
    }
  }


  $menuCheckbox.change(toggleMenu);
}

module.exports = init;