'use strict';

var $ = require('jquery');

function init() {
  var $buttons = $('.js-toggle-map__button');
  var $container = $('.js-toggle-map__cotnainer');

  function clickHandler(e) {
    e.preventDefault();

    $container.toggleClass('active');
  }

  $buttons.click(clickHandler);
}

module.exports = init;