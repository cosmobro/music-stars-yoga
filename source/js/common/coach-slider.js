'use strict';

var $ = require('jquery');

function init() {
  var $slider = $('.js-coach-slider');
  var $sliderTrack = $slider.find('.js-coach-slider__list');
  var $nextButton = $slider.find('.js-coach-slider__nav_next');
  var $prevButton = $slider.find('.js-coach-slider__nav_prev');
  var $coachImage = $('.js-coach-slider__coach-image');

  var sliderOptions = {
    prevArrow: $prevButton,
    nextArrow: $nextButton,
    variableWidth: true,
    centerMode: true,
    slidesToShow: 1,
    speed: 300
  };

  function changeHandler(event, slick, currentSlide, nextSlide) {
    var $nextSlide = slick.$slides.eq(nextSlide);
    var nextCoachImage = $nextSlide.data('coach-image');

    $coachImage.fadeOut(150, function () {
      $coachImage.attr('src', nextCoachImage).fadeIn(150);
    });
  }

  $sliderTrack.slick(sliderOptions);

  $sliderTrack.on('beforeChange', changeHandler);
}

module.exports = init;