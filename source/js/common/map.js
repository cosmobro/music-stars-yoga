'use strict';

var $ = require('jquery');

function createMap(addr) {
  ymaps.ready(function () {
    ymaps.geocode(addr, {
      results: 1
    }).then(function (res) {
      var firstGeoObject = res.geoObjects.get(0);
      var coordinates = firstGeoObject.geometry.getCoordinates();
      var mapCenter = [
        coordinates[0],
        coordinates[1]
      ];
      var mapState = {
        center: mapCenter,
        zoom: 16,
        controls: ['zoomControl']
      };
      var mapOptions = {
        searchControlProvider: 'yandex#search'
      };
      var placemarkProperties = {
        hintContent: addr,
        balloonContent: addr,
        balloonContentHeader: addr
      };
      var placemarkOptions = {
        iconLayout: 'default#image',
        iconImageHref: 'assets/img/marker.png',
        iconImageSize: [60, 72],
        iconImageOffset: [-30, -72]
      };
      var myPlacemark = new ymaps.Placemark(coordinates, placemarkProperties, placemarkOptions);

      window.myMap = new ymaps.Map('map', mapState, mapOptions);

      // Отключения увеличения скроллом
      window.myMap.behaviors.disable('scrollZoom');
      window.myMap.geoObjects.add(myPlacemark);

      // Отключение перетаскивания карты для мобильных устройств
      function mapDragDisable() {
        var minWidth = 1040;
        var mapElement = $('#map');

        if ($(window).width() < minWidth && mapElement.hasClass('draggable')) {
          mapElement.removeClass('draggable');
          myMap.behaviors.disable('drag');
        } else if ($(window).width() > minWidth && !mapElement.hasClass('draggable')) {
          mapElement.addClass('draggable');
          myMap.behaviors.enable('drag');
        }

        // Включаем при тапе по карте
        mapElement.click(function () {
          myMap.behaviors.enable('drag');
        });
      }

      mapDragDisable();
      $(window).resize(mapDragDisable);
    });
  });
}

function destroyMap() {
  if (window.myMap) {
    window.myMap.destroy();
  }
}

function init() {
  window.myMap = null;
  var addr = 'Московская область, Одинцовский район, д. Жуковка, д. 78';


  createMap(addr);
}

module.exports = {
  createMap: createMap,
  destroyMap: destroyMap,
  init: init
};